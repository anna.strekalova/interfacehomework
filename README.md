# InterfaceHomeWork

Приложение для личного финансового учета

3 функции: 
добавить денежную операцию, 
вывести список всех операций
вывести баланс в указанной валюте.
Часть 1
Что есть финансовый учет? Это когда мы следим за своими расходами и доходами. Полезно видеть на что уходят деньги и т.д.
Что такое деньги? Это количество какой-либо валюты. Давайте создадим интерфейс. Назовем его ICurrencyAmount:
string CurrencyCode { get; } 
decimal Amount { get; }
Все расходы, доходы и любые денежные операции - переводы, комиссии - это все транзакции. Каждая транзакция (ITransaction) имеет дату и количество денег с ней связанных:
DateTimeOffset Date { get; } 
ICurrencyAmount Amount { get; }
Давайте создадим интерфейс IBudgetApplication. Не будем заморачиваться с интерфейсом - сделаем консольное приложение. Минимум который будет у нашего приложения - это добавление транзакции, вывод всех транзакций и вывод баланса в заданной валюте.
Соответственно, у него будут следующие методы: 
void AddTransaction(string input); 
void OutputTransactions(); 
void OutputBalanceInCurrency(string currencyCode);
Давайте подумаем, что нам еще нужно из интерфейсов?
Еще нужно где-то транзакции хранить. Для хранения данных существует замечательный паттерн Repository. Давайте применим его. Наверно некоторые сталкивались с ним при работе с БД. Но он специально создан для того, чтобы не зависеть от источника данных. Давайте создадим ITransactionRepository. Удаление и обновление делать не будем. 
void AddTransaction(ITransaction transaction); 
ITransaction[] GetTransactions();
Если есть ввод, значит должен быть и парсер. Давайте создадим интерфейс ITransactionParser: 
ITransaction Parse(string input);
И последнее: надо как-то переводить баланс в другую валюту. Давайте создадим ICurrencyConverter: 
ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode);

Что делаем в части 1?

Читаем задание.
Создаем проект консольное приложение на .NET Core.
Добавляем новые интерфейсы в соответствии с заданием.
IBudgetApplication
ICurrencyAmount
ITransaction
ITransactionRepository
ITransactionParser
ICurrencyConverter

Часть 2
Приступим к наполнению. IBudgetApplication это узел в котором используются все другие интерфейсы. Это наш основной класс, взаимодействующий с пользователем. Создадим его реализацию. Добавим туда зависимости ITransactionRepository transactionRepository, ITransactionParser transactionParser, ICurrencyConverter currencyConverter через конструктор.
Реализуем метод AddTransaction. ITransactionParser берет input и парсит его в транзакцию. ITransactionRepository добавляет транзакцию в хранилище.
OutputTransactions еще проще. Просто берет все транзакции из репозитория и выводит их в консоль.
OutputBalanceInCurrency посложнее. Нужно взять все транзакции, перевести их amounts в заданную валюту и просуммировать.
На эти методы уже можно писать рабочие юнит-тесты замокав зависимости. Опять же мы начали сверху, не углубляясь в детали. Это иногда бывает полезно. На самом деле чем больше потратить времени на проектирование и продумывание вариантов до написания непосредственно кода логики, тем лучше получается конечный результат.
Что делаем в части 2?
Добавляем зависимости существующих интерфейсов в BudgetApplication
Реализуем IBudgetApplication
Часть 3
Что делаем в части 3?

Реализуем интерфейсы данных: ICurrencyAmount (CurrencyAmount), ITransaction (Expense)
Expense содержит дополнительно:
		public string Category { get; }
		public string Destination { get; }   
Реализуем операторы у CurrencyAmount
Реализуем IEquatable<ICurrencyAmount> через hot-key в VS
Реализуем InMemoryTransactionRepository (все транзакции просто хранятся в List<ITransaction>
Реализуем ITransactionParser с методом string.Split, чтобы можно было делать вызывать метод AddTransaction:
"Трата -400 RUB Продукты Пятерочка"
"Трата -2000 RUB Бензин IRBIS"
"Трата -500 RUB Кафе Шоколадница"
Реализуем ICurrencyConverter пока 1 к 1, без изменения количества, меняется только CurrencyCode.
Часть 4
Что делаем в части 4?
Устанавливаем пакет Newtonsoft.Json
Устанавливаем пакет Microsoft.Extensions.Caching.Memory
Реализуем ICurrencyConverter как ExchangeRatesApiConverter со следующими зависимостями:
HttpClient httpClient, IMemoryCache memoryCache, string apiKey
Пишем код запроса к API (https://manage.exchangeratesapi.io/quickstart)
Пишем код кэширования.

Подсказка. Используйте методы:
HttpClient.GetAsync
IMemoryCache.GetOrCreateAsync
ICacheEntry.AbsoluteExpirationRelativeToNow

Реализовать ICurrencyConverter с помощью реализации ExchangeRatesApiConverter: https://exchangeratesapi.io/ (часть 4) API Key: a5cf9da55cb835d0a633a7825b3aa8b5 Или сгенерируйте свой на сайте выше. Для генерации классов C# из ответов в формате JSON использовать сайт: http://quicktype.io/

Расширить TransactionParser для приема транзакции типа Transfer и Income Добавить реализацию ITransactionRepository читающую транзакции из файла и использовать ее в программе. Все транзакции в файле. Одна строка - одна транзакция. Соответственно добавление транзакции равно добавление строки с информацией о ней. Добавить пользовательский ввод в программу. Это значит, что вы можете ввести команду одну из 3-х (add, list, balance) и значение на вход (см. документ с заданием) и программа среагирует соответствующе.





