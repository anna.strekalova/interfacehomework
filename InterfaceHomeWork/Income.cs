﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class Income : ITransaction
    {
        public DateTimeOffset Date { get; }
        public ICurrencyAmount Amount { get; }
        public string Source { get; }

        public Income(DateTimeOffset date, ICurrencyAmount amount, string source)
        {
            this.Date = date;
            this.Amount = amount;
            this.Source = source;
        }

        public override string ToString()
        {
            return $"Зачисление {this.Amount} от {this.Source}.";
        }
    }
}
