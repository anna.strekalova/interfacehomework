﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class Transfer : ITransaction
    {
        public DateTimeOffset Date { get; }
        public ICurrencyAmount Amount { get; }
        public string Message { get; }
        public string Destination { get; }

        public Transfer(DateTimeOffset date, ICurrencyAmount amount, string message, string destination)
        {
            this.Date = date;
            this.Amount = amount;
            this.Message = message;
            this.Destination = destination;
        }

        public override string ToString()
        {
            return $"Перевод {this.Amount} на имя {this.Destination} с сообщением: \"{this.Message}\".";
        }
    }
}