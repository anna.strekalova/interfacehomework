﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class Expense : ITransaction
    {
        public DateTimeOffset Date { get; }
        public ICurrencyAmount Amount { get; }
        public string Category { get; }
        public string Destination { get; }

        public Expense(DateTimeOffset date, ICurrencyAmount amount, string category, string destination)
        {
            this.Date = date;
            this.Amount = amount;
            this.Category = category;
            this.Destination = destination;
        }

        public override string ToString()
        {
            return $"Трата {this.Amount} в {this.Destination} по категории {this.Category}";
        }
    }
}
