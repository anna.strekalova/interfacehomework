﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class CurrencyAmount : ICurrencyAmount, IEquatable<CurrencyAmount>
    {
        public string CurrencyCode { get; }

        public decimal Amount { get; set; }

        public CurrencyAmount(string currencyCode, decimal amount)
        {
            this.CurrencyCode = currencyCode;
            this.Amount = amount;     
        }
        public static CurrencyAmount operator+(CurrencyAmount x, CurrencyAmount y)
        {
            if(x.CurrencyCode != y.CurrencyCode)
            {
                throw new Exception("Коды двух валют должны быть равными.");
            }
            var result = new CurrencyAmount(x.CurrencyCode, x.Amount + y.Amount);

            return result;
        }

        public static CurrencyAmount operator-(CurrencyAmount x, CurrencyAmount y)
        {
            if(x.CurrencyCode != y.CurrencyCode)
            {
                throw new Exception("Коды двух валют должны быть равными.");
            }
            var result = new CurrencyAmount(x.CurrencyCode, x.Amount - y.Amount);

            return result;
        }

        public static bool operator!=(CurrencyAmount x, CurrencyAmount y)
        {
            var result = x.Equals(y);
            return !result;
        }

        public static bool operator ==(CurrencyAmount x, CurrencyAmount y)
        {
            return x.Equals(y);
        }

        public bool Equals(CurrencyAmount other)
        {
            var value = other != null && CurrencyCode == other.CurrencyCode && Amount == other.Amount;
            return value;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CurrencyAmount);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CurrencyCode, Amount);
        }

        public override string ToString()
        {
            return $"Сумма: {this.Amount:0.00} {this.CurrencyCode}";
        }
    }
}
