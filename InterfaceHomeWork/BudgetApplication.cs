﻿using System;

namespace InterfaceHomeWork
{
    public class BudgetApplication : IBudgetApplication
    {
        private ITransactionRepository transactionRepository;
        private ITransactionParser transactionParser;
        private ICurrencyConverter currencyConverter;

        public BudgetApplication(ITransactionRepository transactionRepository, ITransactionParser transactionParser, ICurrencyConverter currencyConverter)
        {
            this.transactionRepository = transactionRepository;
            this.transactionParser = transactionParser;
            this.currencyConverter = currencyConverter;
        }

        public void AddTransaction(string input)
        {
            if(input.Length != 0)
            {
                var transaction = this.transactionParser.Parse(input);
                this.transactionRepository.AddTransaction(transaction);
            }
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            var allTransactions = this.transactionRepository.GetTransactions();
            decimal resultAmount = 0;
            foreach(var transaction in allTransactions)
            {
                var currency = this.currencyConverter.ConvertCurrency(transaction.Amount, currencyCode);
                resultAmount += currency.Amount;
            }
            resultAmount = Decimal.Round(resultAmount, 2);

            Console.WriteLine($"Баланс: {resultAmount} {currencyCode}");
        }

        public void OutputTransactions()
        {
            var transactions = this.transactionRepository.GetTransactions();
            if(transactions.Length < 1)
            {
                Console.WriteLine("Список всех операций пуст.");
                return;
            }

            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction.ToString());
            }
        }
    }
}
