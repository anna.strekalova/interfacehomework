﻿using System;

namespace InterfaceHomeWork
{
    public class TransactionParser : ITransactionParser
    {
        public ITransaction Parse(string input)
        {   
            var date = DateTimeOffset.Now;
            var strTokens = input.Split(' ');
            var operationType = strTokens[0];

            CurrencyAmount currencyAmount = new CurrencyAmount(strTokens[2], decimal.Parse(strTokens[1]));
            switch (operationType)
            {
                case "Трата":
                    return new Expense(date, currencyAmount, strTokens[3], strTokens[4]);
                    break;
                case "Зачисление":
                    return new Income(date, currencyAmount, strTokens[3]);
                case "Перевод":
                    return new Transfer(date, currencyAmount, strTokens[3], strTokens[4]);
                default:
                    throw new NotImplementedException();
            }        
        }
    }
}
