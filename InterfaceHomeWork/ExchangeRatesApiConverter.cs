﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private HttpClient httpClient { get; }
        private IMemoryCache cache { get; }
        private string apiKey { get; }

        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            this.httpClient = httpClient;
            this.cache = memoryCache;
            this.apiKey = apiKey;
        }

        private async Task<ExchangeRatesApiResponse> GetExchangeRatesAsync()
        {
            var response = await this.httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={apiKey}");
            response = response.EnsureSuccessStatusCode();
            var rates = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(rates);
        }

        private async Task<CurrencyAmount> ConvertCurrencyWithCache(ICurrencyAmount amount, string currencyCode)
        {
            var cachedResponse = await this.cache.GetOrCreateAsync("responseKey", cache =>
            {
                cache.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
                return GetExchangeRatesAsync();
            });
            var amountInBase = amount.Amount / (decimal)cachedResponse.Rates[amount.CurrencyCode];
            var amountInTarget = amountInBase * (decimal)cachedResponse.Rates[currencyCode];

            return new CurrencyAmount(currencyCode, amountInTarget);

        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            var result = ConvertCurrencyWithCache(amount, currencyCode).Result;
            return result;
        }
    }


}
