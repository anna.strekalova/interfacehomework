﻿using System.Collections.Generic;
using System.IO;

namespace InterfaceHomeWork
{
    public class FileTransactionRepository : ITransactionRepository
    {
        private List<ITransaction> TransactionsList = new List<ITransaction>();

        public void GetTransactionsFromFile(string path)
        {
            List<ITransaction> transactions = new List<ITransaction>();
            using (StreamReader sr = new StreamReader(path))
            {
                var line = string.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    TransactionParser transactionParser = new TransactionParser();
                    var newTransaction = transactionParser.Parse(line);
                    TransactionsList.Add(newTransaction);
                }
            }
        }

        public void AddTransaction(ITransaction transaction)
        {
            if(transaction != null)
            {
                this.TransactionsList.Add(transaction);
            }
        }

        public ITransaction[] GetTransactions()
        {
            return this.TransactionsList.ToArray();
        }
    }
}
