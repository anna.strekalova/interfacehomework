﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public class InMemoryTransactionRepository : ITransactionRepository
    {
        private List<ITransaction> TransactionList = new List<ITransaction>();

        public void AddTransaction(ITransaction transaction)
        {
            if(transaction != null)
            {
                TransactionList.Add(transaction);
            }
        }

        public ITransaction[] GetTransactions()
        {
            return TransactionList.ToArray();
        }
    }
}
