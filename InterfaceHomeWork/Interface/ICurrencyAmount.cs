﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface ICurrencyAmount
    {
        public string CurrencyCode { get; }
        public decimal Amount { get; }
    }
}
