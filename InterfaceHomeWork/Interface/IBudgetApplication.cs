﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface IBudgetApplication
    {
        public void AddTransaction(string input);
        public void OutputTransactions();
        public void OutputBalanceInCurrency(string currencyCode);
    }
}
