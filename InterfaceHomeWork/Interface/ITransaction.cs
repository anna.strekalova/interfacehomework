﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface ITransaction
    {
        public DateTimeOffset Date { get; }
        public ICurrencyAmount Amount { get; }
    }
}
