﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface ITransactionParser
    {
        public ITransaction Parse(string input);
    }
}
