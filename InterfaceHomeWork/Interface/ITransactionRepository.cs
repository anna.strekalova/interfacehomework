﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface ITransactionRepository
    {
        public void AddTransaction(ITransaction transaction);
        public ITransaction[] GetTransactions();
    }
}
