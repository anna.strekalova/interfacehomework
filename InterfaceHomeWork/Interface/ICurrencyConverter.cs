﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceHomeWork
{
    public interface ICurrencyConverter
    {
        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode);
    }
}
