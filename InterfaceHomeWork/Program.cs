﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Caching.Memory;

namespace InterfaceHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiKey = "a5cf9da55cb835d0a633a7825b3aa8b5";

            var transactionRepo = new FileTransactionRepository();
            var path = $"{Environment.CurrentDirectory}/Transactions.txt";
            transactionRepo.GetTransactionsFromFile(path);

            var transactionParser = new TransactionParser();
            var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), apiKey);
            var budgetApp = new BudgetApplication(transactionRepo, transactionParser, currencyConverter);

            while (true)
             {
                 Console.WriteLine("Введите нужную Вам команду:");
                 Console.WriteLine("\"add\" - добавить денежную операцию");
                 Console.WriteLine("\"list\" - вывести список всех операций");
                 Console.WriteLine("\"balance\" - вывести баланс в указанной валюте");
                 Console.WriteLine("Ввод:");
                 string command = Console.ReadLine();
                 switch (command)
                {
                    case "add":
                        Console.WriteLine("Введите денежную операцию:");
                        var userTransaction = Console.ReadLine();
                        budgetApp.AddTransaction(userTransaction);
                        Console.WriteLine("Денежная операция добавлена.");
                        break;
                    case "list":
                        Console.WriteLine("Список всех операций:");
                        budgetApp.OutputTransactions();
                        break;
                    case "balance":
                        Console.WriteLine("Укажите валюту для вывода баланса:");
                        var userCurrencyCode = Console.ReadLine();
                        budgetApp.OutputBalanceInCurrency(userCurrencyCode);
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда.");
                        break;
                }

                 Console.WriteLine(System.Environment.NewLine);
             }
        }
    }
}
